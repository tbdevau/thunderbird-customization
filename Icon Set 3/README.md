# Icon Set 3

These icons have all been released under the Apache license by their
authors and can be used accordingly.

## Icon Sources
The icons come from the Icon Archive site and have been modified, where
necessary, to fit the requirements for the Thunderbird customisation.

| TB Icon            | Filename           | Source                                  |
| ------------------ | ------------------ | --------------------------------------- |
| Archives           | archive.svg        | Icon Archive: Google                    |
| Trash              | delete.svg         | Icon Archive: Google                    |
| Feed Folder        | feeds-folder.svg   | **MISSING**                             |
| Feed Server        | feeds.svg          | **MISSING**                             |
| Drafts             | file-item.svg      | Icon Archive: Google                    |
| Local Folders      | folder-local.svg   | Icon Archive: Google                    |
| Folder             | folder.svg         | Icon Archive: Google                    |
| Secure News Server | globe-secure.svg   | Icon Archive: Google (combined 2 icons) |
| News server        | globe.svg          | Icon Archive: Google                    |
| Inbox              | inbox.svg          | Icon Archive: Google                    |
| Junk               | junk.svg           | Icon Archive: Google                    |
| Secure Mail Server | message-secure.svg | Icon Archive: Google (combined 2 icons) |
| Mail Server        | message.svg        | Icon Archive: Google                    |
| Newsgroup          | newsgroup.svg      | Icon Archive: Google                    |
| Outbox             | outbox.svg         | Icon Archive: Google                    |
| Search Folder      | search-folder.svg  | Icon Archive: Google (combined 2 icons) |
| Sent               | sent.svg           | Icon Archive: Google                    |
| Templates          | template.svg       | Icon Archive: Google                    |

![Icon Set 3 Screenshot](../Icon-Set-3.png)

## Icon Archive Source URL
- [Google](https://iconarchive.com/artist/google.html)
