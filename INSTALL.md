# Installation Instructions

## Enable Customization In Thunderbird
1. In Thunderbird, go to **Preferences** (**Options** on Windows) &rarr;
**General**,
2. Scroll down and click on the **Config Editor** button,
3. Click on the **I accept the risk!** button,
4. In the **Search** dialog enter
**toolkit.legacyUserProfileCustomizations.stylesheets**,
5. Toggle the value to **True** if it exists or, if not, do the following
6. Right-click on the empty pane and choose **New** &rarr; **Boolean**,
7. Enter **toolkit.legacyUserProfileCustomizations.stylesheets** as the
name and click on **OK**,
8. Now toggle the value to **True**.

## Locate Your Profile Folder
1. In Thunderbird, go to **Account Settings** &rarr; **Server Settings**,
2. Scroll down until you can see the **Local Directory** entry.
3. Make a note of the path to the profile directory (The profile directory
has a name like **gu1ded0g.default-default** where gu1ded0g is some random
set of numbers and letter and **default-default** is the profile name).
4. The profile directory location you need is everything up to **and 
including** the directory name.

e.g.:
On **Windows**, if you have a Windows account name of **JohnDoe** you could
have a profile directory path for an IMAP account of

**C:\Users\JohnDoe\AppData\Roaming\Thunderbird\Profiles\gu1ded0g.default-release\ImapMail\mail.example.com**

or, for a POP account

**C:\Users\JohnDoe\AppData\Roaming\Thunderbird\Profiles\gu1ded0g.default-release\Mail\mail.example.com**

and these would give you a profile directory location of:

**C:\Users\JohnDoe\AppData\Roaming\Thunderbird\Profiles\gu1ded0g.default-release**

On **Linux**, if you have a Linux account name of **johndoe** you could have
a profile directory path for an IMAP account of

**/home/johndoe/.thunderbird/gu1ded0g.tbmisc/ImapMail/mail.example.com**

or, for a POP account

**/home/johndoe/.thunderbird/gu1ded0g.tbmisc/Mail/mail.example.com**

and these would give you a profile directory location of:

**/home/johndoe/.thunderbird/gu1ded0g.tbmisc/**

## Install The CSS File And Icons
1. Open the profile directory you located above,
2. Create a directory called **chrome** (It needs to be all lowercase),
3. Copy the **userChrome.css** file in to the **chrome** directory (Use the **userChrome.css** from the **TB 115 And Above** folder if you are using the latest version of Thunderbird),
4. Copy the SVG icons you want to use in to the **chrome** directory,
5. Quit and restart Thunderbird. The new icons should now be displayed.
