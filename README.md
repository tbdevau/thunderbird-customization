# Thunderbird Customization

## License Information
1. This work is released under the MPL license as contained in the
LICENSE file.
2. The icon sets included with this work are released under their own
license as contained in their individual LICENSE files.

## Important Note
- These customizations only work properly with Thunderbird 78+
- Due to changes in Thunderbird 115+, you **must** use the **userChrome.css**
file in the **TB 115 And Above** folder.

## Known Issue
- If an icon is missing from the installation, it will be displayed as a
blank icon.

## Installation
Read the INSTALL.md file for how to install and enable the customization.

## Icon Set Information
### Icon Set 1
This is a complete set of icons and they have all been released in to the
Public Domain.

![Icon Set 1 Screenshot](Icon-Set-1.png)

### Icon Set 2
This icon set has been release under GPLv3 but I was not able to find
a suitable icon for **Newsgroup (newsgroup.svg)**. You will either need
to remove this entry from the **userChrome.css** file or find an
replacement icon.

![Icon Set 2 Screenshot](Icon-Set-2.png)

### Icon Set 3
This icon set has been released under the Apache licence but I was not
able to find suitable icons for **RSS Feeds Server (feeds.svg)** or
**RSS Feeds Folder (feeds-folder.svg)**. You will either need to remove
these entries from the **userChrome.css** file or find suitable
replacement icons.

![Icon Set 3 Screenshot](Icon-Set-3.png)

## Useful Links
### Mozilla
- [Changing Thunderbirds folder icons](https://support.mozilla.org/en-US/questions/1305856)
- [Change &quot;new mail&quot; color](https://support.mozilla.org/en-US/questions/1261363)
- [userChrome.css has no effect](https://support.mozilla.org/lt/questions/1298376)
- [How can I get userChrome.css to work](https://support.mozilla.org/en-US/questions/1160703)

### Mozillazine
- [How To Customise Firefox 57+ UI with userChrome.css](http://forums.mozillazine.org/viewtopic.php?f=38&t=3037817)
- [UserChrome.css](http://kb.mozillazine.org/index.php?title=UserChrome.css&printable=yes)
- [Searching info about userChrome.css](http://forums.mozillazine.org/viewtopic.php?f=28&t=3070173)
- [Changing folders icons in new Thunderbird](http://forums.mozillazine.org/viewtopic.php?f=30&t=3064381&p=14873377)
- [userChrome.css: How to replace folder icons in folder tree by own folder.svg?](http://forums.mozillazine.org/viewtopic.php?f=39&t=3116701)
