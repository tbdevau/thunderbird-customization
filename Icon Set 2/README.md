# Icon Set 2

These icons have all been released under GPLv3 by their authors and can
be used accordingly.

## Icon Sources
The icons come from either the Icon Archive site or from WikiMedia and
have been modified, where necessary, to fit the requirements for the
Thunderbird customisation.

| TB Icon            | Filename           | Source                                                            |
| ------------------ | ------------------ | ----------------------------------------------------------------- |
| Archives           | archive.svg        | Icon Archive: Sebastian Rubio                                     |
| Trash              | delete.svg         | Icon Archive: Papirus Development Team                            |
| Feed Folder        | feeds-folder.svg   | Icon Archive: Papirus Development Team (combined 2 icons)         |
| Feed Server        | feeds.svg          | Icon Archive: Papirus Development Team                            |
| Drafts             | file-item.svg      | Icon Archive: Papirus Development Team                            |
| Local Folders      | folder-local.svg   | WikiMedia: GNOME Desktop Icons                                    |
| Folder             | folder.svg         | Icon Archive: sora-meliae                                         |
| Secure News Server | globe-secure.svg   | Icon Archive: Papirus Development Team (combined 2 icons)         |
| News server        | globe.svg          | Icon Archive: Papirus Development Team                            |
| Inbox              | inbox.svg          | Icon Archive: sora-meliae                                         |
| Junk               | junk.svg           | WikiMedia: Faenza                                                 |
| Secure Mail Server | message-secure.svg | Icon Archive: sora-meliae &amp; Papirus Development Team combined |
| Mail Server        | message.svg        | Icon Archive: sora-meliae                                         |
| Newsgroup          | newsgroup.svg      | **MISSING**                                                       |
| Outbox             | outbox.svg         | Icon Archive: sora-meliae                                         |
| Search Folder      | search-folder.svg  | WikiMedia: GNOME Desktop Icons                                    |
| Sent               | sent.svg           | Icon Archive: sora-meliae                                         |
| Templates          | template.svg       | Icon Archive: Papirus Development Team                            |

![Icon Set 2 Screenshot](../Icon-Set-2.png)

## Icon Source URLs
### Icon Archive
- [Sebastian Rubio](https://iconarchive.com/artist/sbstnblnd.html)
- [Papirus Development Team](https://iconarchive.com/artist/papirus-team.html)
- [sora-meliae](https://iconarchive.com/artist/sora-meliae.html)
### WikiMedia
- [GNOME Desktop Icons](https://commons.wikimedia.org/wiki/GNOME_Desktop_icons)
- [Faenza](https://commons.wikimedia.org/wiki/File:Faenza-mail-mark-junk.svg)
