# Icon Set 1

These icons have all been released in to the public domain by their
authors and can be used without restriction accordingly.

## Icon Sources
The icons come from either the Open Clipart site or the Tango icon
collection and have been modified, where necessary, to fit the
requirements for the Thunderbird customisation.

| TB Icon            | Filename           | Source                                                       |
| ------------------ | ------------------ | ------------------------------------------------------------ |
| Archives           | archive.svg        | Open Clipart Artist: SixSixFive (Removed the star)           |
| Trash              | delete.svg         | Open Clipart Artist: DooFi: Modified to remove the arrow     |
| Feed Folder        | feeds-folder.svg   | Open Clipart Artist: SixSixFive (Combined two icons)         |
| Feed Server        | feeds.svg          | Open Clipart Artist: SixSixFive                              |
| Drafts             | file-item.svg      | Tango Icon Collection                                        |
| Local Folders      | folder-local.svg   | Tango Icon Collection                                        |
| Folder             | folder.svg         | Tango Icon Collection                                        |
| Secure News Server | globe-secure.svg   | Tango Icon Collection (with padlock from another Tango Icon) |
| News server        | globe.svg          | Tango Icon Collection                                        |
| Inbox              | inbox.svg          | Open Clipart Artist: Anonymous                               |
| Junk               | junk.svg           | Open Clipart Artist: gsantner                                |
| Secure Mail Server | message-secure.svg | Tango Icon Collection (with padlock from another Tango Icon) |
| Mail Server        | message.svg        | Tango Icon Collection                                        |
| Newsgroup          | newsgroup.svg      | Tango Icon Collection                                        |
| Outbox             | outbox.svg         | Open Clipart Artist: Chrisdesign                             |
| Search Folder      | search-folder.svg  | Tango Icon Collection                                        |
| Sent               | sent.svg           | Open Clipart Artist: danilo                                  |
| Templates          | template.svg       | Tango Icon Collection                                        |

![Icon Set 1 Screenshot](../Icon-Set-1.png)

## Icon Source URLs
- [Tango Icon Collection](http://tango.freedesktop.org/Tango_Desktop_Project)
- [Open Clipart](http://openclipart.org/)
